from ExtractionTray import get_tray_4_angles, find_angulars, select_angles, find_angulars_manualy
from homography import *
from BlocksExtraction import *
from CsvGenerator import csvGenerator
from CsvLEANER import CsvCLEANER
import os

def checkFileExistance(filePath):
    try:
        with open(filePath, 'r') as f:
            return True
    except FileNotFoundError as e:
        return False
    except IOError as e:
        return False

def checkFormatJPG(filePath):
    extension=filePath.split('.')[-1]
    if extension == "jpg" or extension == "JPG":
        return True
    return False


def main():
    print("Hello user.")
    while True:
        # Choice of the image to process
        setup = input("Which setup ?\n>>")
        mistake = False
        if setup == "quit":
            print("Closing the app...\nGood bye user.")
            return
        if setup == "?":
            print("Liste of the commands :")
            print(">>?  -- Print the app help.")
            print(">>quit -- Close the app.")
            print(">>path2file -- Image processing on the image path2file.")
            mistake = True
            continue
        if not os.path.exists(setup):
            print("Error !\nThis file doesn't exist.")
            mistake = True
            continue

        if not mistake:
            # Choice of the tray coordinates
            print()
            print("The standard dimensions of the tray are (114.2, 62.2)")
            confirm = input("Confirm ?  y/n\n")
            if confirm == 'n':
                length = float(input("Length of the tray ? "))
                print()
                width = float(input("Width of the tray ? "))
                print()
            else:
                length = 114.2
                width = 62.2

        # Now we have check that the file exist
        # Now let's check if it is a jpg file.
        # if not checkFormatJPG(img):
        #     print("Error !\nIncorrect format. The specified file has to to be extended by .jpg or .JPG")
        #     continue
        print("Image Processing ...")
        print(">> Tray extraction ...")
        first_image = True
        for img in os.listdir(setup):
            if os.path.isfile(os.path.join(setup, img)):
                img = setup + '/' + img
                image = cv2.imread(img)
                liste_corners = []
                if first_image:
                    angles = select_angles(img)
                    first_image = False
                for i, (x, y) in enumerate(angles):
                    print("Point {} : (".format(i), x, ", ", y, ")")
                    x = round(x)
                    y = round(y)
                    cv2.circle(image, (x, y), 25, (0, 0, 255), -1)
                    liste_corners.append((x, y))

                cv2.namedWindow('Detected Angulars', cv2.WINDOW_NORMAL)
                cv2.resizeWindow('Detected Angulars', 600, 400)


                cv2.imshow("Detected Angulars", image)
                cv2.waitKey(500)


                confirm = None
                while  not (confirm == "y" or confirm == "n"):
                    confirm = input("Confirm ?  y/n\n")
                cv2.destroyWindow('Detected Angulars')
                if confirm == "n":
                    liste_corners = find_angulars_manualy(cv2.imread(img))
                    liste_corners = [(c.x, c.y) for c in liste_corners]
                angles = liste_corners

                print("Corners successfully saved.")
                print(">> Homography...")
                homog(liste_corners, img)
                print("New image successfully saved.")
                print(">> Blocks hunting ...")

                path_homog = "homog_results/Homog_" + img.split('/', 1)[1]
                list_cercles = extract(path_homog)
                print(">> Filling csv file ...")
                csvGenerator(list_cercles, liste_corners, img, length, width)
                print("Done")

        print("# --- CsvLEANER --- #\n")

        path_data = "../DATA/sorted_raw_data/"
        directory = os.fsencode(path_data)

        csvFile = os.fsencode(img.split('/')[1] + ".csv")
        CsvCLEANER(directory, csvFile)

        print("{} -- CLEANED\n".format(csvFile))


main()
