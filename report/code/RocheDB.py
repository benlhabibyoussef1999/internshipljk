# Import zone
import os
import re
import csv
import json
import sqlite3


def checkSyntaxFilename(filename):
  '''
    Check if the syntax of the file is correct using filenameSyntax.json.
  '''
  with open('filenameSyntax.json', 'r') as json_file:
    syntax = json.load(json_file)
    lst = filename.split('_')
    if (len(lst) != 8):
      print('Error: The following filename is not formated properly {}'.format(filename))
      return False
    for i in range(len(lst)):
      if (i == 0 and int(lst[i]) not in syntax["j_num_obj"]):
        print("Error: Filename syntax invalid, --num_obj-- not recognized or approved for file {}".format(filename))
        return False
      if (i == 1 and lst[i] not in syntax["j_obj"]):
        print("Error: Filename syntax invalid, --obj-- not recognized or approved for file {}".format(filename))
        return False
      if (i == 2 and lst[i] not in syntax["j_sorted"]):
        print("Error: Filename syntax invalid, --sorted-- not recognized or approved for file {}".format(filename))
        return False
      if (i == 3 and lst[i] not in syntax["j_slope_mat"]):
        print("Error: Filename syntax invalid, --slope_mat-- not recognized or approved for file {}".format(filename))
        return False
      if (i == 4 and lst[i] not in syntax["j_arr_mat"]):
        print("Error: Filename syntax invalid, --arr_mat-- not recognized or approved for file {}".format(filename))
        return False
      if (i == 5 and (int(lst[i]) < syntax["j_angle"][0] or int(lst[i]) > syntax["j_angle"][1])):
        print("Error: Filename syntax invalid, --angle-- not recognized or approved for file {}".format(filename))
        return False
      if (i == 6 and (int(lst[i]) < syntax["j_height"][0] or int(lst[i]) > syntax["j_height"][1])):
        print("Error: Filename syntax invalid, --height-- not recognized or approved for file {}".format(filename))
        return False
  return True


def filenameToInfo(filename):
  '''
    Return a list of the informations in the filename.
    The list will be as follow: [num_obj, obj, sorted, slope_mat, arr_mat, angle, clean.csv]
  '''
  filename.replace('.csv', '')
  lst = filename.split('_')
  return lst


def maxSetup(data=True):
  '''
    Return the biggest setup number in the table data.
    Exit if the table data is empty as there's no max setup number to return.
    [WARNING]: A connexion must have been established with the DB before using this function.
  '''
  if data:
    req_max_number_setup = 'SELECT MAX(setup) FROM data'
  else:
    req_max_number_setup = 'SELECT MAX(setup) FROM stat'
  cursor.execute(req_max_number_setup)
  setup = cursor.fetchone()[0]  # fetchone returns a tupple
  
  if (setup is None):
    print('Error: This database is empty')
    exit()

  setup += 1
  return setup


def checkEndDB(dbName):
  '''
    Check if the file extension is missing from the DB name given by the user.
  '''
  if (not dbName.endswith('.sqlite')):
    dbName = dbName + '.sqlite'
  return dbName


def atoi(text):
  '''
    Convert the string text to an integer if the string is a number. 
  '''
  return int(text) if text.isdigit() else text


def natural_keys(text):
  '''
    Generate the natural keys to sort in human order.
    alist.sort(key=natural_keys) sorts in human order.
  '''
  return [ atoi(c) for c in re.split(r'(\d+)', text) ]


def addNewElementsToData(setup, directory, dbName):
  '''
    Add the content of every clean CSV file that follows the syntax to the table 'data' of the DB.
  '''

  # For each field added to the DB, you need to add '?' in the following line

  req_insertion = 'INSERT INTO data VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
  json_file = open('filenameSyntax.json', 'r')
  syntax = json.load(json_file)

  # Create or open context.txt
  path_context = "../DATA/data_base/"
  if (setup == 1):
      context_file = open(path_context + "context_" + dbName.replace(".sqlite", "") + ".txt", 'w')
      context_file.write('---- Setup number to full filename ----\n\n')
  elif (setup > 1):
      context_file = open(path_context + "context_" + dbName.replace(".sqlite", "") + ".txt", 'a')
  
  # # Sort the list of files in the directory
  # dir = os.listdir(directory)
  # dir.sort(key = natural_keys)

  while True:
    # Choice of the CSV file
    print()
    print("-- Adding content of clean CSV file to the table 'data' --\n")
    clean_csv = input("Please input a name for the clean CSV file: ")
    if clean_csv == "quit":
      return

    if (clean_csv.endswith("_clean.csv")):
        if (not checkSyntaxFilename(clean_csv)):
            print("- {} -- IGNORED".format(clean_csv))
        else:
            context_file.write('{} -- {}'.format(setup, clean_csv))
            context_file.write('\n')

            completeFilename = directory + clean_csv
            insert_in_db = []

            print("The standard dimensions of the tray are (114, 62)")
            confirm = input("Confirm ?  y/n\n")
            if confirm == 'n':
              LDS = float(input("Length of the deposite surface ? "))
              print()
              WDS = float(input("Width of the deposite surface ? "))
              print()
            else:
              LDS = 114
              WDS = 62

            print("The protective edge is equal to 0")
            confirm = input("Confirm ?  y/n\n")
            if confirm == 'n':
              PE = float(input("Protective Edge ? "))
              print()
            else:
              PE = 0

            with open(completeFilename, 'r') as f:
                csv_reader = csv.reader(f, delimiter=' ')
                infoLst = filenameToInfo(clean_csv)
                if (infoLst[2] == 'loose'):
                    sor = 0
                elif (infoLst[2] == 'neat'):
                    sor = 1

                # For each throw
                first = True
                for row in csv_reader:
                    if first:
                        first = False
                        continue
                    else:
                        num_obj = int(row[1])
                        num_throw = int(row[0])
                        num_oob = int(row[2])
                        angle = int(infoLst[5])
                        height = int(infoLst[6])
                        # Gather the bricks' coordinates and other info
                        for i in range(3, num_obj + 3):
                            # (setup, num_throw, br_id, coor_x, coor_y, num_obj, num_oob, angle, sorted, obj, slope_mat, arr_mat)
                            insert_in_db.append((setup, num_throw, i - 2, row[i], row[i + num_obj], num_obj, num_oob, angle, height, LDS, WDS, PE, sor, syntax["j_obj"].index(infoLst[1]), syntax["j_slope_mat"].index(infoLst[3]), syntax["j_arr_mat"].index(infoLst[4])))
            cursor.executemany(req_insertion, insert_in_db)
            setup+=1

            print("- {} -- ADDED TO DB".format(clean_csv))
    else:
        print("This file doesn't exist\n")


    # Push the changes to the DB
    conn.commit()

  conn.close()
  context_file.close()


def addNewElementsToStat(setup, directory):
  '''
    Add the content of every CSV file that folows the correct patern to the table 'stat' of the DB.
  '''
  req_insertion = 'INSERT INTO stat VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'

  # Sort the list of files in the directory
  dir = os.listdir(directory)
  dir.sort(key = natural_keys)

  for filename in dir:
    if (filename.endswith("_stat.csv")):
      print("- {} -- ADDED TO DB".format(filename))
      completeFilename = directory + filename
      insert_in_db = []

      with open(completeFilename, 'r') as f:
        csv_reader = csv.reader(f, delimiter=',')
        
        # For each throw
        num_throw = 0
        for row in csv_reader:
          # The first lign is the column title
          if (num_throw != 0):
            row = [float(i) for i in row]
            row.insert(0, num_throw)
            row.insert(0, setup)
            
            insert_in_db.append(tuple(row))
          num_throw += 1
      cursor.executemany(req_insertion, insert_in_db)      
      setup+=1
    else:
      print("- {} -- IGNORED".format(filename))

  # Push the changes to the DB
  conn.commit()
  conn.close()
  exit()
    

# --- MAIN ---
print("# --- RocheDB --- #")
print()
print("[0] Create the table data on the DB from CSV files")
print("[1] Add new data to an existing data table on the DB from CSV files")
print("[2] Create the stat table in an existing DB from CSV files")
print("[3] Add new stat to an existing stat table on the DB from CSV files")
print("[4] Exit")
print()
choice = input("Please select an option [0-4]: ")


if(choice == '0'):
  print()
  print("-- Data Table Creation --")

  dbName = input("Please input a name for the DB: ")

  dbName = checkEndDB(dbName)
  path_db = "../DATA/data_base/" + dbName

  # If a DB with this name exist delete it
  # otherwise there will be an error during the creation of the DB
  if (os.path.isfile(path_db)):
    os.remove(path_db)

  # To add a field in the DB, you need to add the corresponding line in the following request

  # Create the table
  req_table_creation = '''CREATE TABLE data
                          (setup INTEGER NOT NULL,
                           num_throw INTEGER NOT NULL,
                           br_id INTEGER NOT NULL,
                           coor_x REAL NOT NULL,
                           coor_y REAL NOT NULL,
                           num_obj INTEGER NOT NULL,
                           num_oob INTEGER NOT NULL,
                           angle INTEGER NOT NULL,
                           height INTEGER NOT NULL,
                           LDS INTEGER NOT NULL,
                           WDS INTEGER NOT NULL,
                           PE INTEGER NOT NULL,
                           sorted INTEGER NOT NULL CHECK (sorted=0 OR sorted=1),
                           obj INTEGER NOT NULL,
                           slope_mat INTEGER NOT NULL,
                           arr_mat INTEGER NOT NULL,
                           PRIMARY KEY (setup, num_throw, br_id)
                          )'''
  path_db = "../DATA/data_base/"
  conn = sqlite3.connect(path_db + dbName)
  cursor = conn.cursor()
  cursor.execute(req_table_creation)

  path = "../DATA/clean_data/"

  directory = os.fsencode(path)

  addNewElementsToData(1, directory.decode(), dbName)


elif (choice == '1'):
  print()
  print("-- Data Table Update --")

  # Connect to the DB
  dbName = input("Please input the name of the database to open: ")

  dbName = checkEndDB(dbName)
  path_db = "../DATA/data_base/" + dbName

  if (not os.path.isfile(path_db)):
    print("Error: the database file doesn't exist")
    exit()
  
  conn = sqlite3.connect(path_db)
  cursor = conn.cursor()

  path = "../DATA/clean_data/"
  directory = os.fsencode(path)


  addNewElementsToData(maxSetup(), directory.decode(), dbName)


elif (choice == '2'):
  print()
  print("-- Stat Table Creation --")

  # Connect to the DB
  dbName = input("Please input the name of the database to open: ")

  dbName = checkEndDB(dbName)
  path_db = "../DATA/data_base/" + dbName

  if (not os.path.isfile(path_db)):
    print("Error: the database file doesn't exist")
    exit()
  
  conn = sqlite3.connect(path_db)
  cursor = conn.cursor()

   # Create the table
  req_table_creation = '''CREATE TABLE stat
                          (setup INTEGER NOT NULL,
                           num_throw INTEGER NOT NULL,
                           min REAL NOT NULL,
                           max REAL NOT NULL,
                           mean REAL NOT NULL,
                           "25q" REAL,
                           "50q" REAL,
                           "75q" REAL,
                           "88q" REAL,
                           "91q" REAL,
                           "25q_oob" REAL,
                           "50q_oob" REAL,
                           "75q_oob" REAL,
                           "88q_oob" REAL,
                           "91q_oob" REAL,
                           cor_coef REAL,
                           pval_shap_norm REAL,
                           pval_lillie_norm REAL,
                           pval_ks_gamma REAL,
                           pval_shap_boxcox REAL,
                           prop_oob REAL,
                           prob_oob_ks_gamma REAL,
                           prob_oob_boxcox REAL,
                           PRIMARY KEY (setup, num_throw)
                          )'''
  cursor.execute(req_table_creation)

  path = "../DATA/basic_statistics/"
  directory = os.fsencode(path)

  addNewElementsToStat(1, directory.decode())


elif (choice == '3'):
  print()
  print("-- Stat Table Update --")

  # Connect to the DB
  dbName = input("Please input the name of the database to open: ")

  dbName = checkEndDB(dbName)
  path_db = "../DATA/data_base/" + dbName

  if (not os.path.isfile(path_db)):
    print("Error: the database file doesn't exist")
    exit()
  
  conn = sqlite3.connect(path_db)
  cursor = conn.cursor()

  path = "../DATA/basic_statistics/"
  directory = os.fsencode(path)


  addNewElementsToStat(maxSetup(data=False), directory.decode())


elif (choice == '4' or int(choice) not in range(4+1)):
  exit()