# File organisation
What is the content of the different folders ?

* **sorted_raw_data**: This folder contains the original .CSV files that were given to me at the beginning of the internship, they contain the coordinates of the objects and some other data.
The .CSV files have been renamed so we can easily see in which conditions the data have been collected.

* **clean_data**: This folder contains the clean .CSV files produced by the script ```CsvLEANER.py```.
They are the same files you can find in ```sorted_raw_data``` except that all the information that are not required to create the table *data* in the database have been removed.

* **estimation_oob_data**: This folder contains .CSV files produced by the R script ```3_estimations_probability_oob.R```. 
These .CSV files contain various estimations for an object to go out of bound (P(X > 60)) for each throw in a setup, as well as other related information.
The first number in the file name corresponds to the setup number, you can find which setup number correspond to which file by reading the file ```data_base/context.txt```.

* **basic_statistics**: This folder contains .CSV files produced by the R script ```4_stat_table_data.R```.
These .CSV files contain various statistical informations about each throw in a setup, they are required by the script ```RocheDB.py```, to create the *stat* table in the database.

* **data_base**: This folder contains the SQLite database (chute_de_bloc.sqlite) and the context.txt file.
The present database is up to date to the latest data and it contains both the *data* and *stat* tables. 

## Comments 
One .CSV file equals one setup.

**[WARNING]**: If you add new data to the database, you will need to regenerate (or at least rename) all (or most) of the .CSV files in ```estimation_oob_data``` and ```basic_statistics```, as the setup numbers may change, so make sure to keep an eye on ```context.txt```.
