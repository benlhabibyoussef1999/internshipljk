from ExtractionTray import get_tray_4_angles, find_angulars, select_angles, find_angulars_manualy
from homography import *
from BlocksExtractionImage import *
from CsvGenerator import csvGenerator
import os

def checkFileExistance(filePath):
    try:
        with open(filePath, 'r') as f:
            return True
    except FileNotFoundError as e:
        return False
    except IOError as e:
        return False

def checkFormatJPG(filePath):
    extension=filePath.split('.')[-1]
    if extension == "jpg" or extension == "JPG":
        return True
    return False


def main():
    print("Hello user.")
    while True:
        # Choice of the image to process
        img = input("Which image ?\n>>")
        if img == "quit":
            print("Closing the app...\nGood bye user.")
            return
        if img == "?":
            print("Liste of the commands :")
            print(">>?  -- Print the app help.")
            print(">>quit -- Close the app.")
            print(">>path2file -- Image processing on the image path2file.")
            continue
        if not checkFileExistance(img):
            print("Error !\nThis file doesn't exist.")
            continue
        # Now we have check that the file exist
        # Now let's check if it is a jpg file.
        if not checkFormatJPG(img):
            print("Error !\nIncorrect format. The specified file has to to be extended by .jpg or .JPG")
            continue
        print("Image Processing ...")
        print(">> Tray extraction ...")
        image = cv2.imread(img)
        liste_corners = []
        angles = select_angles(img)
        # for i, (x, y) in enumerate(get_tray_4_angles(img)):
        for i, (x, y) in enumerate(angles):
            print("Point {} : (".format(i), x, ", ", y, ")")
            x = round(x)
            y = round(y)
            cv2.circle(image, (x, y), 25, (0, 0, 255), -1)
            liste_corners.append((x, y))

        cv2.namedWindow('Detected Angulars', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('Detected Angulars', 600, 400)


        cv2.imshow("Detected Angulars", image)
        cv2.waitKey(1000)


        confirm = None
        while  not (confirm == "y" or confirm == "n"):
            confirm = input("Confirm ?  y/n\n")
        cv2.destroyWindow('Detected Angulars')
        if confirm == "n":
             liste_corners = find_angulars_manualy(cv2.imread(img))
             liste_corners = [(c.x, c.y) for c in liste_corners]

        print("Corners successfully saved.")
        print(">> Homography...")
        homog(liste_corners, img)
        print("New image successfully saved.")
        print(">> Blocks hunting ...")

        path_homog = "homog_results/Homog_" + img.split('/', 1)[1]
        list_cercles = extract(path_homog)
        print(">> Filling csv file ...")
        csvGenerator(list_cercles, angles, img)
        print("Done")





main()
