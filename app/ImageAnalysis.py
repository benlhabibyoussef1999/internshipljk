import cv2
import numpy as np



def empty(a):
    pass

img = "homog_results/Homog_28_brickFull_loose_wood_wood_45_8/18.JPG"

color_infos = (0, 255, 255)

cv2.namedWindow("TrackBars")
cv2.resizeWindow("TrackBars", 600, 400)
cv2.createTrackbar("Hue Min", "TrackBars", 0, 105, empty)
cv2.createTrackbar("Hue Max", "TrackBars", 105, 105, empty)
cv2.createTrackbar("Sat Min", "TrackBars", 148, 255, empty)
cv2.createTrackbar("Sat Max", "TrackBars", 255, 255, empty)
cv2.createTrackbar("Val Min", "TrackBars", 72, 255, empty)
cv2.createTrackbar("Val Max", "TrackBars", 255, 255, empty)

while True:

    image = cv2.imread(img)
    image=cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    h_min = cv2.getTrackbarPos("Hue Min", "TrackBars")
    h_max = cv2.getTrackbarPos("Hue Max", "TrackBars")
    s_min = cv2.getTrackbarPos("Sat Min", "TrackBars")
    s_max = cv2.getTrackbarPos("Sat Max", "TrackBars")
    v_min = cv2.getTrackbarPos("Val Min", "TrackBars")
    v_max = cv2.getTrackbarPos("Val Max", "TrackBars")

    print(h_min, h_max, s_min, s_max, v_min, v_max)

    lower = np.array([h_min, s_min, v_min])
    upper = np.array([h_max, s_max, v_max])
    mask=cv2.inRange(image, lower, upper)

    image2=cv2.bitwise_and(image, image, mask=mask)
    elements=cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]


    if len(elements) > 0:
        blocks = []
        for e in elements:
            if e.size > 0:
                blocks.append(e)
        cpt = 0
        for block in blocks:
            ((x, y), rayon)=cv2.minEnclosingCircle(block)
            # print(len(ejectas))
            if rayon>30:
                cv2.circle(image2, (int(x), int(y)), int(rayon), color_infos, 2)
                cv2.circle(image, (int(x), int(y)), int(rayon), color_infos, 2)
                cpt += 1
        print(cpt)

    cv2.imshow('Image', cv2.resize(image, (600, 400)))
    cv2.imshow('image2', cv2.resize(image2, (600, 400)))
    cv2.imshow('Mask', cv2.resize(mask, (600, 400)))
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cv2.destroyAllWindows()