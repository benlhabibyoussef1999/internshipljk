#!/usr/bin/env python



import cv2
import numpy as np
import os
from math import pi

EPS = 0.01


class Cercle:
    def __init__(self, x, y, r=50):
        self.x = x
        self.y = y
        self.r = r
        self.s = int(pi * r * r)

    def __eq__(self, other):
        a = (abs(self.x -other.x) <= EPS )
        b = (abs(self.y -other.y) <= EPS )
        c = (abs(self.r -other.r) <= EPS )
        return (a and b and c)

    def distance(self, other):
        return ((self.x-other.x)**2 + (self.y-other.y)**2)**(0.5)

    def cercles_dans(self, liste):
        """
        Retourne True si un cercle de liste a son milieu à l'interieur du cercle self.
        """
        for c in liste:
            d = self.distance(c)
            if d < self.r:
                return True
        return False

    def nombre_cercles_dans(self, liste):
        """
        Retourne le nombre de cercles de liste dont le centre est dans self.
        """
        res = 0
        for c in liste:
            d = self.distance(c)
            if d < self.r:
                res += 1
        return res


def divide_large_circles(liste_cercles_nouv):
    cercles_nouv = []
    for cercle in liste_cercles_nouv:
        if cercle.s > 12000 and cercle.s != 12887:
            cercle_1 = Cercle(cercle.x - cercle.r / 2, cercle.y + cercle.r / 2, cercle.r / 2)
            cercle_2 = Cercle(cercle.x + cercle.r / 2, cercle.y - cercle.r / 2, cercle.r / 2)
            cercles_nouv.extend([cercle_1, cercle_2])
        elif cercle.s > 400:
            cercles_nouv.append(cercle)
    return cercles_nouv




img_circles = None
img_wc = None
circles = []


def modify_circles_manualy(image, list_circles, image_without_circles):
    global img_circles
    img_circles = image
    global img_wc
    img_wc = image_without_circles
    global circles
    circles = list_circles
    # displaying the image
    cv2.namedWindow('Modifying circles', cv2.WINDOW_NORMAL)
    cv2.imshow('Modifying circles', img_circles)

    # setting mouse hadler for the image
    # and calling the click_event() function
    cv2.setMouseCallback('Modifying circles', click_event)

    # wait for a key to be pressed to exit
    cv2.waitKey(0)
    # close the window
    cv2.destroyAllWindows()
    return (img_circles, circles)


def click_event(event, x, y, flags, params):
    global img_circles
    # checking for right mouse clicks
    if event == cv2.EVENT_RBUTTONDOWN:
        # Deleting circle
        c = sorted(circles, key=lambda circle: (circle.x - x) ** 2 + (circle.y - y) ** 2)[0]
        circles.remove(c)
        # displaying the new image window without the circle
        img_circles = draw_circles(img_wc, circles)
        cv2.imshow('Modifying circles', img_circles)

    # checking for left mouse clicks
    if event == cv2.EVENT_LBUTTONDOWN:
        # Adding circle
        c = Cercle(x, y)
        circles.append(c)
        # displaying the new circle on the image window
        img_circles = draw_circles(img_wc, circles)
        cv2.imshow('Modifying circles', img_circles)




def draw_circles(res, liste_cercles_nouv):
    res_copy = res.copy()
    for c in liste_cercles_nouv:
        cv2.circle(res_copy, (int(c.x), int(c.y)), int(c.r), (0, 255, 255), 2)
        # cv2.putText(res_copy, str(c.s), (int(c.x) - 35, int(c.y) + 17), cv2.FONT_HERSHEY_SIMPLEX, .8, (0, 255, 255),
        #             thickness=2)

    cv2.putText(res_copy, str(len(liste_cercles_nouv)), (1600, 400), cv2.FONT_HERSHEY_SIMPLEX, 8, (255, 0, 0), thickness=4)
    return res_copy

class ExtractionBlocs:
    def __init__(self, img):
        self.VERBOSE = False
        self.image = cv2.imread(img)
        self.rayon_minimum = 29
        self.erosion = 1 #3
        self.dilatation = 4 #4
        self.lo = np.array([9, 120, 90])#np.array([0, 150, 50])
        self.hi = np.array([12, 180, 200])#np.array([179, 200, 300])
        self.noyau_erosion = np.ones((5,5),np.uint8)
        self.noyau_dilatation = np.ones((5,5),np.uint8)
        self.tolerance_rayon = 0
        self.k = 1
        self.seuil_canny_bas = 0
        self.seuil_canny_haut = 50
        self.suppression_cercles = True
        self.dilatation_cercles = 1
        self.dilatation_canny = 1


    def save_img(self, img, titre):
        if self.VERBOSE:
            cv2.imwrite("results/" +titre+".jpg", img)

    def seuillage_init(self, img):
        #return cv2.inRange(cv2.cvtColor(img, cv2.COLOR_BGR2HSV), self.lo, self.hi)
        return cv2.threshold(cv2.split(cv2.cvtColor(img, cv2.COLOR_BGR2HSV))[1],135,255,cv2.THRESH_BINARY)[1]

    def inv_pixels(self, img):
        """
        Inverse les pixels de img (le noir devient blanc et le blanc devient noir.)
        """
        img_c = img.copy()
        img[img_c == 0] = 255
        img[img_c == 255] = 0

    def erosion_(self, img):
        return cv2.erode(img,self.noyau_erosion,iterations = self.erosion)

    def dilatation_(self, img):
        return cv2.dilate(img,self.noyau_dilatation,iterations = self.dilatation)

    def generer_cercles(self, masque):
        res = []
        contours = cv2.findContours(masque, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]
        for elem in contours:
            ((x, y), rayon) = cv2.minEnclosingCircle(elem)
            res.append(Cercle(x, y, rayon))
        return res

    def filtrer_petits_rayons(self, liste_cercles):
        i = len(liste_cercles)
        while i != 0:
            elem = liste_cercles[i-1]
            if elem.r < self.rayon_minimum + self.tolerance_rayon:
                liste_cercles.remove(elem)
            i = i - 1

    def filtrer_cercles_imbiques(self, liste_cercles, liste_cercles_init):
        hyper = 95
        nouv = []
        for i in range(len(liste_cercles)):
            b = True
            for j in range(len(liste_cercles)):
                if liste_cercles[i] != liste_cercles[j]:
                    d = liste_cercles[i].distance(liste_cercles[j])
                    if d > max(liste_cercles[i].r, liste_cercles[j].r) :
                        continue
                    else:
                        if liste_cercles[i].r < liste_cercles[j].r:
                            b = self.fonction_alex(liste_cercles[i], liste_cercles, liste_cercles_init) # b = False
                            break
            if b:
                nouv.append(liste_cercles[i])
        return nouv

    def fonction_alex(self, petit_cercle,liste_cercles, liste_cercles_init):
        """
        Retroune True si le petit_cercle est le deuxième plus grand cercle à l'interieur d'un grand cercle de liste_cercles_init
        """
        return False


    def ajouter_cercles_oublies(self, liste_cercles_nouv, liste_cercles_init):
        nouv = liste_cercles_nouv.copy()
        for alex in liste_cercles_init:
            if alex.cercles_dans(liste_cercles_nouv):
                pass
            else:
                nouv.append(alex)
        return nouv

    def nouv_f(self, liste_cercles_final, liste_cercles_init, liste_cercles_interm):
        res = liste_cercles_final.copy()
        hyper = 95
        for c_init in liste_cercles_init:
            if c_init.r >= hyper:
                # 2 cas possibles, 1 cercle dedans ou plus
                if c_init.nombre_cercles_dans(liste_cercles_final) >= 2:
                    pass
                else:
                    # 1 cercle, je dois ajouter le deuxième.
                    n_liste = []
                    for c in liste_cercles_interm:
                        d = c_init.distance(c)
                        if d < c_init.r:
                            n_liste.append(c)
                    if len(n_liste) >=2:
                        n_liste.sort(reverse=True, key=lambda cer: cer.r)
                        res.append(n_liste[1])
        return res

    def nouv_ff(self, liste_c):
        hyper = 50
        eps = 10
        res = []
        d = []
        for i in range(len(liste_c)):
            for j in range(i+1, len(liste_c)):
                c1 = liste_c[i]
                c2 = liste_c[j]
                if c1.distance(c2) < c1.r+c2.r + eps and c1.r < hyper and c2.r < hyper:
                        res.append(Cercle((c1.x+c2.x)/2, (c1.y+c2.y)/2, c1.r+c2.r) )
                        d.append(c1)
                        d.append(c2)
        for c in liste_c:
            test = True
            for g in d:
                if g == c:
                    test = False
            if test:
                res.append(c)
        return res


    def main(self, image, nb_ejectas):

        img_file = image
        img = self.image
        # cv2.imshow("0", cv2.resize(img, (600, 400)))
        # cv2.waitKey(1000)
        seuil = self.seuillage_init(img)
        # cv2.imshow("1", cv2.resize(seuil, (600, 400)))
        # cv2.waitKey(1000)
        self.save_img(seuil, "Threshold")

        erode = self.erosion_(seuil)
        # cv2.imshow("2", cv2.resize(erode, (600, 400)))
        # cv2.waitKey(1000)
        dilate = self.dilatation_(erode)
        # cv2.imshow("3", cv2.resize(dilate, (600, 400)))
        # cv2.waitKey(1000)
        self.save_img(erode, "Erosion")
        self.save_img(seuil, "Dilatation")

        masque = cv2.bitwise_and(img, img, mask=erode)
        # cv2.imshow("4", cv2.resize(masque, (600, 400)))
        # cv2.waitKey(1000)
        self.save_img(dilate, "Mask")

        liste_cercles_init = self.generer_cercles(erode)
        self.filtrer_petits_rayons(liste_cercles_init)

        prem_res = img.copy()
        for c in liste_cercles_init:
            cv2.circle(prem_res, (int(c.x), int(c.y)), int(c.r), (0, 255, 255), 2)
        self.save_img(prem_res, "ResInit")
        # cv2.imshow("5", cv2.resize(prem_res, (600, 400)))
        # cv2.waitKey(1000)

        masque_gris = cv2.cvtColor(masque, cv2.COLOR_BGR2GRAY)
        self.save_img(masque_gris, "GrayMask")
        # cv2.imshow("6", cv2.resize(masque_gris, (600, 400)))
        # cv2.waitKey(1000)

        if self.k != 1:
            masque_gris=cv2.blur(masque_gris, (self.k, self.k))
            self.save_img(masque_gris, "BlurGrayMask")

        img_canny = cv2.Canny(masque_gris, self.seuil_canny_bas, self.seuil_canny_haut)
        self.save_img(img_canny, "Canny")
        # cv2.imshow("7", cv2.resize(img_canny, (600, 400)))
        # cv2.waitKey(1000)

        if self.suppression_cercles:
            cimg = cv2.cvtColor(img_canny,cv2.COLOR_GRAY2BGR)
            img_canny_sans_cercles = np.zeros_like(img_canny)
            cercles_a_supprimer = cv2.HoughCircles(img_canny,cv2.HOUGH_GRADIENT,1,11, param1=5,param2=12,minRadius=0,maxRadius=8)
            if cercles_a_supprimer is not None:
                circles = np.uint16(np.around(cercles_a_supprimer))
                for i in circles[0,:]:
                    # draw the outer circle
                    cv2.circle(img_canny_sans_cercles,(i[0],i[1]),i[2], 255)
                    cv2.circle(cimg,(i[0],i[1]),i[2],(0,255,0),2)
                # cv2.imshow("8", cv2.resize(img_canny_sans_cercles, (600, 400)))
                # cv2.waitKey(1000)
                img_canny_sans_cercles = cv2.dilate(img_canny_sans_cercles,np.ones((5,5),np.uint8),iterations = self.dilatation_cercles)
                # cv2.imshow("9", cv2.resize(img_canny_sans_cercles, (600, 400)))
                # cv2.waitKey(1000)
            self.inv_pixels(img_canny_sans_cercles)
            # cv2.imshow("9.5", cv2.resize(img_canny_sans_cercles, (600, 400)))
            # cv2.waitKey(1000)
            img_canny = cv2.bitwise_and(img_canny, img_canny, mask=img_canny_sans_cercles)
            if self.VERBOSE:
                self.save_img(img_canny, "CannyWhithoutCercle")
            if self.VERBOSE:
                self.save_img(cimg, "DetectedCircles")
            # cv2.imshow("10", cv2.resize(img_canny, (600, 400)))
            # cv2.waitKey(1000)

            img_canny_dilatee = cv2.dilate(img_canny,np.ones((5,5),np.uint8),iterations = self.dilatation_canny)
            # cv2.imshow("11", cv2.resize(img_canny_dilatee, (600, 400)))
            # cv2.waitKey(1000)
            self.inv_pixels(img_canny_dilatee)
            # cv2.imshow("11.5", cv2.resize(img_canny_dilatee, (600, 400)))
            # cv2.waitKey(1000)
            self.save_img(img_canny_dilatee, "DilatedCannyFilter")

        nouv_filtre = cv2.bitwise_and(masque, masque, mask=img_canny_dilatee)
        self.save_img(nouv_filtre, "NewMask")
        # cv2.imshow("12", cv2.resize(nouv_filtre, (600, 400)))
        # cv2.waitKey(1000)

        gris = cv2.cvtColor(nouv_filtre,cv2.COLOR_BGR2GRAY)
        seuillage_ = cv2.threshold(gris,0,255,cv2.THRESH_BINARY_INV+cv2.THRESH_OTSU)[1]
        self.inv_pixels(seuillage_)

        self.save_img(seuillage_, "NewThreshold")
        # cv2.imshow("13", cv2.resize(seuillage_, (600, 400)))
        # cv2.waitKey(1000)


        if self.suppression_cercles:
            img_canny_sans_cercles_ = np.zeros_like(seuillage_)
            if cercles_a_supprimer is not None:
                circles = np.uint16(np.around(cercles_a_supprimer))
                for i in circles[0,:]:
                    # draw the outer circle
                    cv2.circle(img_canny_sans_cercles_,(i[0],i[1]),i[2], 255, -1)
                # cv2.imshow("14", cv2.resize(img_canny_sans_cercles_, (600, 400)))
                # cv2.waitKey(1000)
                img_canny_sans_cercles_ = cv2.dilate(img_canny_sans_cercles_,np.ones((5,5),np.uint8),iterations = self.dilatation_cercles)
                # cv2.imshow("15", cv2.resize(img_canny_sans_cercles_, (600, 400)))
                # cv2.waitKey(1000)
            seuillage_ = cv2.addWeighted(seuillage_,1,img_canny_sans_cercles_,1,0)
            # cv2.imshow("16", cv2.resize(seuillage_, (600, 400)))
            # cv2.waitKey(1000)
            if self.VERBOSE:
                self.save_img(seuillage_, "NouveauSeuillageSansCercles")


        param1 = 1
        # seuillage_ = cv2.dilate(seuillage_,np.ones((5,5),np.uint8),iterations = param1)
        # cv2.imshow("17", cv2.resize(seuillage_, (600, 400)))
        # cv2.waitKey(1000)
        if param1 > 0:
            self.save_img(seuillage_,"NouveauSeuillageErod")



        liste_cercles_nouv = self.generer_cercles(seuillage_)
        cliste_cercles_nouv = liste_cercles_nouv.copy()

        second_res = img.copy()
        for c in liste_cercles_nouv:
            cv2.circle(second_res, (int(c.x), int(c.y)), int(c.r), (0, 255, 255), 2)


        # filtrage des petites cercles
        self.filtrer_petits_rayons(liste_cercles_nouv)

        third_res = img.copy()
        for c in liste_cercles_nouv:
            cv2.circle(third_res, (int(c.x), int(c.y)), int(c.r), (0, 255, 255), 2)

        # filtrage des cercles imbriqués
        liste_cercles_nouv = self.filtrer_cercles_imbiques(liste_cercles_nouv, liste_cercles_init)

        fourth_res = img.copy()
        for c in liste_cercles_nouv:
            cv2.circle(fourth_res, (int(c.x), int(c.y)), int(c.r), (0, 255, 255), 2)

        # cercles oubliés
        liste_cercles_nouv = self.ajouter_cercles_oublies(liste_cercles_nouv, liste_cercles_init)

        fifth_res = img.copy()
        for c in liste_cercles_nouv:
            cv2.circle(fifth_res, (int(c.x), int(c.y)), int(c.r), (0, 255, 255), 2)

        # nouv
        liste_cercles_nouv = self.nouv_f(liste_cercles_nouv, liste_cercles_init, cliste_cercles_nouv)

        sixth_res = img.copy()
        for c in liste_cercles_nouv:
            cv2.circle(sixth_res, (int(c.x), int(c.y)), int(c.r), (0, 255, 255), 2)

        # liste_cercles_nouv = self.nouv_ff(liste_cercles_nouv)

        seventh_res = img.copy()
        for c in liste_cercles_nouv:
            cv2.circle(seventh_res, (int(c.x), int(c.y)), int(c.r), (0, 255, 255), 2)

        res = img.copy()

        cv2.destroyAllWindows()

        if nb_ejectas > 0:
            liste_cercles_nouv = sorted(liste_cercles_nouv, key=lambda cercle: cercle.x)[:-nb_ejectas]

        liste_cercles_nouv = divide_large_circles(liste_cercles_nouv)

        res = draw_circles(res, liste_cercles_nouv)

        cv2.namedWindow('Detected Blocks', cv2.WINDOW_NORMAL)
        cv2.resizeWindow('Detected Blocks', 600, 400)

        cv2.imshow("Detected Blocks", res)
        cv2.waitKey(1)

        change = False
        while len(liste_cercles_nouv) != int(img_file.split('_')[2]) - nb_ejectas:
            change = True
            print("Some blocks are incorrectly detected")
            print("Use the right mouse button to add new circles or the left button to remove misplaced ones")
            print("Then click Enter\n")
            (res, liste_cercles_nouv) = modify_circles_manualy(res, liste_cercles_nouv, img)

        while not change:
            print("You can modify the detected circles or accept them and continue")
            # modify = None
            # while not (modify == "y" or modify == "n"):
            #     modify = input("Modify ?  y/n\n")
            #
            # if modify == "y":
            (res, liste_cercles_nouv) = modify_circles_manualy(res, liste_cercles_nouv, img)
            if len(liste_cercles_nouv) != int(img_file.split('_')[2]) - nb_ejectas:
                print("Are you sure you made the correct changes ?")
                print("You have another chance !!\n")
            else:
                print("The blocks seem well detected\n")
                change = True


        # cv2.imshow("first", cv2.resize(prem_res, (600, 400)))
        # cv2.imshow("second", cv2.resize(second_res, (600, 400)))
        # cv2.imshow("third", cv2.resize(third_res, (600, 400)))
        # cv2.imshow("fourth", cv2.resize(fourth_res, (600, 400)))
        # cv2.imshow("fifth", cv2.resize(fifth_res, (600, 400)))
        # cv2.imshow("sixth", cv2.resize(sixth_res, (600, 400)))
        # cv2.imshow("seventh", cv2.resize(seventh_res, (600, 400)))
        # cv2.imshow("last", cv2.resize(res, (600, 400)))


        cv2.waitKey(500)
        cv2.destroyAllWindows()

        path_result = "results/FinalResult_" + image.split('/')[1].split('_', 1)[1]
        if not os.path.exists(path_result):
            print("{} -- CREATED \n".format("FinalResult_" + image.split('/')[1].split('_', 1)[1]))
            os.makedirs(path_result)
        cv2.imwrite(path_result + '/' + image.split('/')[2], res)
        return liste_cercles_nouv


def ejectas(img):
    lo = np.array([24, 90, 75])
    hi = np.array([105, 255, 255])

    image = cv2.imread(img)
    image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(image, lo, hi)
    image2 = cv2.bitwise_and(image, image, mask=mask)
    elements = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)[-2]

    if len(elements) == 0:
        return 0
    ejectas = []
    for e in elements:
        if e.size > 150:
            ejectas.append(e)
    return len(ejectas)


def extract(img):
    extraction = ExtractionBlocs(img)
    nb_ejectas = ejectas(img)
    print("{} ejectas are detected\n".format(nb_ejectas))
    confirm = input("Confirm ?  y/n\n")
    if confirm == 'n':
        nb_ejectas = int(input("Number of ejectas ? "))
        print()
    return extraction.main(img, nb_ejectas)
