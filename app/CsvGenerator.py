# Import zone
import os
import csv


def csvFileExist(csvFile, directory):
    for filename in os.listdir(directory):
        if filename == csvFile:
            return True
    return False

def csvcount(filename):
    with open(filename, 'r') as f:
        i = 0
        for ligne in f:
            i += 1
    return i

def csvGenerator(list_cercles, angles, img, l, w):
    print("# --- CsvGenerator --- #")
    print()

    imgFile = os.fsencode(img.split('/', 1)[1])

    if (imgFile.startswith(b"28")):
        nb_bricks = 28
    elif (imgFile.startswith(b"14")):
        nb_bricks = 14
    elif (imgFile.startswith(b"40")):
        nb_bricks = 40
    elif (imgFile.startswith(b"42")):
        nb_bricks = 42
    elif (imgFile.startswith(b"1")):
        nb_bricks = 1
    elif (imgFile.startswith(b"29")):
        nb_bricks = 29
    else:
        print("Error: the number of objects thrown in a file as not been configured.")
        exit()

    path_data = "../DATA/sorted_raw_data/"
    directory = os.fsencode(path_data)

    imgFileCsv = os.fsencode(img.split('/')[1] + ".csv")
    csvFile = directory + imgFileCsv

    new = False
    if not os.path.exists(csvFile):
        print("{} -- CREATED \n".format(imgFileCsv))
        new = True

    with open(csvFile, 'a') as f_out:
        csv_writer = csv.writer(f_out, delimiter=' ')
        if new:
            colValues = []
            colValues.extend(["nÂ", "nb_briques", "nb_ejectat", "XcoinHG1", "YcoinHG1", "XcoinHD2", "YcoinHD2", "XcoinBD3", "YcoinBD3", "XcoinBG4", "YcoinBG4", "Xbriques"])
            for i in range(nb_bricks - 1):
                colValues.append("")
            colValues.append("Xejectnul")
            for i in range(nb_bricks - 1):
                colValues.append("")
            colValues.append("Ybriques")
            for i in range(nb_bricks - 1):
                colValues.append("")
            colValues.append("Yejectnul")
            for i in range(nb_bricks - 1):
                colValues.append("")
            csv_writer.writerow(colValues)
        print("{} -- UPDATED\n".format(imgFileCsv))
        colValues = []
        if new:
            colValues.extend([1, nb_bricks, max(nb_bricks - len(list_cercles), 0), angles[0][0], angles[0][1], angles[1][0], angles[1][1], angles[2][0], angles[2][1], angles[3][0], angles[3][1]])
        else:
            colValues.extend([csvcount(csvFile), nb_bricks, max(nb_bricks - len(list_cercles), 0), angles[0][0], angles[0][1], angles[1][0], angles[1][1], angles[2][0], angles[2][1], angles[3][0], angles[3][1]])
        length = max(angles[1][0] - angles[0][0], angles[2][0] - angles[3][0])
        width = max(angles[2][1] - angles[1][1], angles[3][1] - angles[0][1])
        for idx_cercle in range(min(len(list_cercles), nb_bricks)):
            colValues.append(max(list_cercles[idx_cercle].x * (114.2 / length), 0))
        for _ in range(len(list_cercles), nb_bricks):
            colValues.append(999)
        for _ in range(nb_bricks):
            colValues.append(0)
        for idx_cercle in range(min(len(list_cercles), nb_bricks)):
            colValues.append(max(list_cercles[idx_cercle].y * (62.2 / width), 0))
        for _ in range(len(list_cercles), nb_bricks):
            colValues.append(999)
        for _ in range(nb_bricks):
            colValues.append(0)
        csv_writer.writerow(colValues)
