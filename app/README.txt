App utilisation

First the list of usefull libraries is saved in the file requirements.txt.
To download these libraries, you should write :
-> pip install -r requirements.txt

To launch the app :
-> python3 main.py

Then the app will ask you which setup you want to process. You have to write the path to the setup.
For example :
-> tests/28_brickFull_loose_wood_wood_45_8

When adding a setup with a number of blocks different then the existing possible values (1, 14, 28, 40, 42 till now), 
you need to add lines of code in:
- CsvGenerator.py: between line 34 and 35.
- CsvLEANER.py: between line 25 and line 26
and add the number in the list "j_num_obj" in the file "filenameSyntax.json" from the directory "DB_Creator".
  
