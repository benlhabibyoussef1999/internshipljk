#!/usr/bin/env python
import cv2

img_corners = None
img_wc = None
liste_corners = []

import cv2
import numpy as np
from skimage.morphology import skeletonize
import skimage.measure
from BlocksExtraction import Cercle

def select_angles(path):
    if path.split('/')[1] == "14_brickFull_loose_wood_wood_45_21":
        return [(740, 490), (3090, 320), (3180, 2260), (920, 2400)]
    elif path.split('/')[1] == "28_brickFull_loose_wood_wood_45_11":
        return [(79, 580), (3594, 505), (3590, 2350), (186, 2461)]
    elif path.split('/')[1] == "28_brickFull_loose_wood_wood_30_21":
        return [(84, 518), (3554, 549), (3479, 2354), (146, 2363)]
    elif path.split('/')[1] == "1_brickFull_loose_wood_wood_45_21":
        return [(338, 338), (3642, 330), (3639, 2163), (304, 2126)]
    elif path.split('/')[1] == "29_dice4_loose_wood_wood_45_21":
        return [(585, 496), (3078, 513), (3078, 1909), (488, 1898)]
    else:
        return [(740, 490), (3090, 320), (3180, 2260), (920, 2400)]

def get_tray_4_angles(path):
    img = cv2.imread(path, 3)
    cv2.imshow("img", cv2.resize(img, (600, 400)))
    cv2.waitKey(1000)
    w_, h_, _ = img.shape
    #     resize the image in order to reduce time of working and also to bring all images to the same size:
    #     the smaller border = 1000 pixels
    print("h_, w_", h_, ", ", w_, ")")
    if (w_ > h_):
        w = int(1000 * w_ / h_)
        h = 1000
    else:
        w = 1000
        h = int(1000 * h_ / w_)
    print("h, w", h, ", ", w, ")")
    img = cv2.resize(img, (h, w))
    img_copy = img.copy()

    #     use Canny to obtain borders
    bords = cv2.Canny(img, 120, 200)
    cv2.imshow("bords", cv2.resize(bords, (600, 400)))
    cv2.waitKey(1000)
    kernel = np.ones((4, 4), np.uint8)

    #     dilatate to reduce affect of small occasional pieces of boards
    fin = cv2.dilate(bords, kernel, iterations=2)
    cv2.imshow("dilate", cv2.resize(fin, (600, 400)))
    cv2.waitKey(1000)

    #     use Probabilistic Hough to get the most probable line segments
    lines = cv2.HoughLinesP(fin, 1, np.pi / 360, threshold=50, minLineLength=200, maxLineGap=10)

    #     create black image in order to after draw all line segment on it and get binary image(0,1)
    black = np.zeros(img.shape)
    cpt = 1
    for x1, y1, x2, y2 in np.array(lines[:, 0]):
        #         if (x1 != 0  and y1!= 0  and x2 != 0  and y2!= 0):
        cv2.line(img_copy, (x1, y1), (x2, y2), (0, 0, 255), 5, cv2.LINE_AA)
        cv2.line(black, (x1, y1), (x2, y2), (1, 1, 1), 5, cv2.LINE_AA)
        cv2.imshow("img_copy_" + str(cpt), cv2.resize(img_copy, (600, 400)))
        cv2.waitKey(200)
        cpt += 1
    cv2.imshow("img_copy", cv2.resize(img_copy, (600, 400)))
    cv2.waitKey(1000)
    cv2.imshow("black", cv2.resize(black, (600, 400)))
    cv2.waitKey(1000)
    black_copy = black.copy()

    im = black.copy()[:, :, 0]

    #     label image to get each connected region
    labeled_image = skimage.measure.label(im, connectivity=1, return_num=True)

    d = {}
    #     find all connected regions crossed by the edge of image and remove it
    #     if the number of pixels with this label is not the biggest
    if labeled_image[1] > 1:
        for i in range(labeled_image[1] + 1):
            d.update({len(labeled_image[0][labeled_image[0] == i]): i})
        d = dict(sorted(d.items()))
        l = []
        for k, v in d.items():
            l.append(v)
        bad_set = set()
        for x in range(im.shape[0]):
            if labeled_image[0][x, 0] != l[-2] and labeled_image[0][x, 0] != 0:
                bad_set.add(labeled_image[0][x, 0])
            elif labeled_image[0][x, -1] != l[-2] and labeled_image[0][x, -1] != 0:
                bad_set.add(labeled_image[0][x, -1])
        for y in range(im.shape[1]):
            if labeled_image[0][0, y] != l[-2] and labeled_image[0][0, y] != 0:
                bad_set.add(labeled_image[0][x, 0])
            elif labeled_image[0][-1, y] != l[-2] and labeled_image[0][-1, y] != 0:
                bad_set.add(labeled_image[0][x, -1])
        for lab in bad_set:
            for x in range(im.shape[0]):
                for y in range(im.shape[1]):
                    if labeled_image[0][x, y] == lab:
                        im[x, y] = 0

    #    use skeletonizing to decrease uncertainty before the second applying of the Hough:
    #     on this step we have already obtained the confidence tray edges regions
    out = skeletonize(im > 0)
    out = 255 * (out.astype(np.uint8))

    #     second use of Hough transform
    lines = cv2.HoughLines(out, 1, np.pi / 360, 130)
    img_colour = np.dstack([im, im, im])
    cpt = 1
    for rho, theta in lines[:, 0]:
        a = np.cos(theta)
        b = np.sin(theta)
        x0 = a * rho
        y0 = b * rho
        x1 = int(x0 + 2000 * (-b))
        y1 = int(y0 + 2000 * (a))
        x2 = int(x0 - 2000 * (-b))
        y2 = int(y0 - 2000 * (a))
        cv2.line(img_colour, (x1, y1), (x2, y2), (0, 0, 255), 2)
        # cv2.imshow("img_colour_" + str(cpt), cv2.resize(img_colour, (600, 400)))
        # cv2.waitKey(200)
        # cpt += 1
    cv2.imshow("img_colour", cv2.resize(img_colour, (600, 400)))
    cv2.waitKey(1000)
    #     find points of intersection
    pts = []
    for i in range(lines.shape[0]):
        (rho1, theta1) = lines[i, 0]
        for j in range(i + 1, lines.shape[0]):
            (rho2, theta2) = lines[j, 0]
            if abs(theta1 - np.pi / 2) <= 1e-8:
                line1 = LineString([(rho1, 0), (rho1, image.shape[1] / 2)])
                line2 = LineString([(rho2, 0), (0, rho2 * np.sin(theta2))])
                intersection = line1.intersection(line2)
                pts.append(intersection.x, intersection.y)
                continue
            if abs(theta2 - np.pi / 2) <= 1e-8:
                line1 = LineString([(rho2, 0), (rho2, image.shape[1] / 2)])
                line2 = LineString([(rho1, 0), (0, rho1 * np.sin(theta1))])
                intersection = line1.intersection(line2)
                pts.append(intersection.x, intersection.y)
                continue
            m1 = -1 / np.tan(theta1)
            c1 = rho1 / np.sin(theta1)
            m2 = -1 / np.tan(theta2)
            c2 = rho2 / np.sin(theta2)
            if np.abs(m1 - m2) <= 1e-5 or np.abs(theta1 - theta2) < np.pi / 2 - np.pi / 36:
                continue
            x = (c2 - c1) / (m1 - m2)
            y = m1 * x + c1
            if 0 <= x < img.shape[1] and 0 <= y < img.shape[0]:
                pts.append((int(x), int(y)))

    #     Find convex hull
    pts = np.array(pts)
    pts = pts[:, None]
    hull = cv2.convexHull(pts)

    #     Sometimes after using hull we still obtain not only one point for each angle, but after some observations
    #     we get that it is mistakes and found how to adress it
    for i in range(len(hull[:, 0])):
        for j in range(i + 1, len(hull[:, 0])):
            point1 = hull[:, 0][i]
            point2 = hull[:, 0][j]
            if abs(point1[0] - point2[0]) < 100 and abs(point1[1] - point2[1]) < 100:
                if (point1[0] > im.shape[0] / 2):
                    hull[:, 0][i][0] = max(point1[0], point2[0])
                    hull[:, 0][j][0] = max(point1[0], point2[0])
                else:
                    hull[:, 0][i][0] = min(point1[0], point2[0])
                    hull[:, 0][j][0] = min(point1[0], point2[0])
                if (point1[1] > im.shape[1] / 2):
                    hull[:, 0][i][1] = max(point1[1], point2[1])
                    hull[:, 0][j][1] = max(point1[1], point2[1])
                else:
                    hull[:, 0][i][1] = min(point1[1], point2[1])
                    hull[:, 0][j][1] = min(point1[1], point2[1])
            elif abs(point1[1] - point2[1]) < 100 and abs(point1[0] - point2[0]) < 100:
                if (point1[0] > im.shape[0] / 2):
                    hull[:, 0][i][1] = max(point1[1], point2[1])
                    hull[:, 0][j][1] = max(point1[1], point2[1])
                else:
                    hull[:, 0][i][1] = min(point1[1], point2[1])
                    hull[:, 0][j][1] = min(point1[1], point2[1])
                if (point1[1] > im.shape[1] / 2):
                    hull[:, 0][i][1] = max(point1[1], point2[1])
                    hull[:, 0][j][1] = max(point1[1], point2[1])
                else:
                    hull[:, 0][i][1] = min(point1[1], point2[1])
                    hull[:, 0][j][1] = min(point1[1], point2[1])

    hull = cv2.convexHull(hull)

    #     finally we didn't need k-means
    # Step #5 - K-Means clustering
    # Define criteria = ( type, max_iter = 10 , epsilon = 1.0 )
    criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)

    # Set flags (Just to avoid line break in the code)
    flags = cv2.KMEANS_RANDOM_CENTERS

    # Apply KMeans
    # The convex hull points need to be float32
    z = hull.copy().astype(np.float32)
    compactness, labels, centers = cv2.kmeans(z, 4, None, criteria, 10, flags)

    # Step #6 - Find the lengths of each side
    centers = cv2.convexHull(centers)[:, 0]

    cv2.destroyAllWindows()
    return (centers / (w / w_))


def find_angulars(img):
    return [(3532, 400), (3533, 2279), (184, 2272), (102, 399)]

def draw_circles(img, list_corners):
    img_copy = img.copy()
    for c in list_corners:
        cv2.circle(img_copy, (int(c.x), int(c.y)), int(c.r), (0, 0, 255), -1)

    return img_copy


def click_event(event, x, y, flags, params):
    # checking for left mouse clicks
    if event == cv2.EVENT_LBUTTONDOWN:
        # displaying the coordinates
        # on the Shell
        print(x, ' ', y)
        # Adding circle
        c = Cercle(x, y, 25)
        liste_corners.append(c)
        # displaying the new circle on the image window
        img_corners = draw_circles(img_wc, liste_corners)
        cv2.imshow('Corners finding', img_corners)

    # checking for right mouse clicks
    if event == cv2.EVENT_RBUTTONDOWN:
        # Deleting circle
        c = sorted(liste_corners, key=lambda circle: (circle.x - x) ** 2 + (circle.y - y) ** 2)[0]
        liste_corners.remove(c)
        # displaying the new image window without the circle
        img_corners = draw_circles(img_wc, liste_corners)
        cv2.imshow('Corners finding', img_corners)


def find_angulars_manualy(image):
    """
    To do
    """
    global img_corners
    img_corners = image
    global img_wc
    img_wc = image
    global liste_corners
    liste_corners = []
    # reading the image

    # displaying the image
    cv2.namedWindow('Corners finding', cv2.WINDOW_NORMAL)
    cv2.imshow('Corners finding', img_corners)

    # setting mouse hadler for the image
    # and calling the click_event() function
    cv2.setMouseCallback('Corners finding', click_event)

    # wait for a key to be pressed to exit
    cv2.waitKey(0)
    # close the window
    cv2.destroyAllWindows()
    return liste_corners[:4]


