# Import zone
import os
import csv

def CsvCLEANER(directory, fileToClean):
    if (fileToClean.startswith(b"28")):
        unwanted = [3, 4, 5, 6, 7, 8, 9, 10, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66]
        limit = 94
        nb_bricks = 28
    elif (fileToClean.startswith(b"14")):
        unwanted = [3, 4, 5, 6, 7, 8, 9, 10, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38]
        limit = 52
        nb_bricks = 14
    elif (fileToClean.startswith(b"40")):
        unwanted = [3, 4, 5, 6, 7, 8, 9, 10]
        limit = 90
        nb_bricks = 40
    elif (fileToClean.startswith(b"42")):
        unwanted = [3, 4, 5, 6, 7, 8, 9, 10]
        limit = 94
        nb_bricks = 42
    elif (fileToClean.startswith(b"1")):
        unwanted = [3, 4, 5, 6, 7, 8, 9, 10, 12]
        limit = 13
        nb_bricks = 1
    elif (fileToClean.startswith(b"29")):
        unwanted = [3, 4, 5, 6, 7, 8, 9, 10, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 68]
        limit = 97
        nb_bricks = 29
    else:
        print("Error: the number of objects thrown in a file as not been configured.")
        exit()

    path_clean = "../DATA/clean_data/"

    directory_clean = os.fsencode(path_clean)
    cleanFile = directory_clean + fileToClean.replace(b'.csv', b'_clean.csv')
    fileToClean = directory + fileToClean
    j = 0

    with open(fileToClean, 'r') as f_in, open(cleanFile, 'w') as f_out:
        csv_reader = csv.reader(f_in, delimiter=' ')
        csv_writer = csv.writer(f_out, delimiter=' ')

        for row in csv_reader:
            colValues = []
            if j == 0:
                colValues.extend(["nÂ", "nb_briques", "nb_ejectat", "Xbriques"])
                for i in range(nb_bricks - 1):
                    colValues.append("")
                colValues.append("Ybriques")
                for i in range(nb_bricks - 1):
                    colValues.append("")
                csv_writer.writerow(colValues)
                j += 1
            else:
                for i in range(0, len(row)):
                    if (i > limit):
                        csv_writer.writerow(colValues)
                        break
                    elif (i not in unwanted):
                        value = round(float(row[i]), 2)
                        if value % 1 != 0:
                            colValues.append(value)
                        else:
                            colValues.append(int(value))


# # --- MAIN ---
# print("# --- CsvLEANER --- #")
# print()
#
# path = "../DATA/sorted_raw_data/"
#
# directory = os.fsencode(path)
#
# suffix = ".csv"
# count = 0
# for filename in os.listdir(directory):
#   if filename.endswith(b".csv"):
#       CsvCLEANER(directory, filename)
#       print("{}# {} -- CLEANED".format(count, filename))
#       count+=1
#
# print()
# print("All the CSV files in the given directory have been cleaned")