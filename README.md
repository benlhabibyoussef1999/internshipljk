# File organisation
What is the content of the different folders ?

* **app**: This folder contains the application code (the main program to execute is main.py), as well as the repositories of the images to be processed, the intermediate results of the homography and the final results.

* **DATA**: This folder contains mainly the data base, the original and clean CSV files.

* **DB_Creator**: This folder contains the code used to create a new database or add new features to an already existing database.

* **report**: This folder contains the report in pdf and tex version, as well as the resources file and the images used.
